const express = require("express");
const Bookmark = require("../models/bookmarks");

// Creación del router
const router = express.Router();

//mostrar todos los bookmarks
router.get("/bookmarks/:id_usuario", async (req, res) => {
  try {
    var idUser = req.params.id_usuario;
    const bookmarks_db = await Bookmark.find({ id_dueno: idUser }); //JSON de bookmarks
    //const num_book = bookmarks_db; //cantidad de bookmarks

    // Retorna el objeto guardado al cliente
    res.json(bookmarks_db); //tal vez seria mejor poner un aviso de que fue creado correctamente
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

//ver un solo bookmark
router.get("/bookmarks/:id_usuario/:id_book", async (req, res) => {
  try {
    const idUser = req.params.id_usuario
    const idBook = req.params.id_book
    const bookmark_db = await Bookmark.find({ id_dueno: idUser, _id: idBook }).exec(); //JSON de bookmarks

    // Retorna el objeto guardado al cliente
    res.json(bookmark_db); //tal vez seria mejor poner un aviso de que fue creado correctamente
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

//crear un bookmark
router.post("/bookmarks/:id_usuario", async (req, res) => {
  try {
    var idUser = req.params.id_usuario;
    const nuevoBookmark = new Bookmark({
      nombre: req.body.nombre,
      url: req.body.url,
      comentario: req.body.comentario,
      user_book: req.body.user_book,
      pass_book: req.body.pass_book,
      id_dueno: idUser,
      id_label: req.body.id_label //lo borre por disculpas xd
    });
    // Guarda el objeto en BD
    let resultado = await nuevoBookmark.save();
    // Retorna el objeto guardado al cliente
    res.json(resultado); //tal vez seria mejor poner un aviso de que fue creado correctamente
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

//eliminar un bookmark
router.delete("/bookmarks/:id_usuario/:id_book", async (req, res) => {
  // Obtiene el id
  const idUser = req.params.id_usuario
  const idBook = req.params.id_book
  try {
    // Elimina la tarea por el id
    const resultado = await Bookmark.deleteOne({ id_dueno: idUser, _id: idBook  })
    // Retorna la respuesta al cliente
    res.json(resultado);
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

//editar un bookmark
router.patch("/bookmarks/:id_usuario/:id_book", async (req, res) => {
  // Obtiene el id de la tarea
  const idUser = req.params.id_usuario
  const idBook = req.params.id_book

  try {
    // Actualiza la tarea en la BD
    const resultado = await Bookmark.updateOne(
      { id_dueno: idUser, _id: idBook },
      {
        $set: req.body,
      }
    );
    // Retorna la respuesta al cliente
    return res.json(resultado);
  } catch (error) {
    return res.status(500).send(error);
  }
});

// ver los bookmarks de un label
router.get("/bookmarks/porlabel/:id_usuario/:id_label", async (req, res) => {

  var idUser = req.params.id_usuario;
  var idLabel = req.params.id_label;
 // res.send({idUser, idLabel})
  
  try {
    const bookmarks_db = await Bookmark.find({ id_dueno: idUser, id_label: idLabel }); 
    // Retorna el objeto guardado al cliente
    res.json(bookmarks_db); //tal vez seria mejor poner un aviso de que fue creado correctamente
  } catch (error) {
    // retorna el error al cliente
    res.status(500).send(error);
  }
});

module.exports = router;
