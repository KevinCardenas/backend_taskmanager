const express = require("express");
const Label = require("../models/labels")

// Creación del router
const router = express.Router();

//mostrar todos los labels de un usuario
router.get("/labels/:id_usuario", async (req, res) => {
    try {
        var idUser = req.params.id_usuario;
        const labels_db = await Label.find({ id_dueno: idUser }).exec();
        // Retorna el objeto guardado al cliente
        res.json(labels_db); //tal vez seria mejor poner un aviso de que fue creado correctamente
    } catch (error) {
        // retorna el error al cliente
        res.status(500).send(error);
    }
});

//ver un solo label
router.get("/labels/:id_usuario/:id_label", async (req, res) => {
    try {
        const idUser = req.params.id_usuario
        const idLabel = req.params.id_label
        const label_db = await Label.find({ id_dueno: idUser, _id: idLabel }).exec();
        // Retorna el objeto guardado al cliente
        res.json(label_db); //tal vez seria mejor poner un aviso de que fue creado correctamente
    } catch (error) {
        // retorna el error al cliente
        res.status(500).send(error);
    }
});

//crear un label
router.post("/labels/:id_usuario", async (req, res) => {
    try {
        var idUser = req.params.id_usuario;
        const nuevoLabel = new Label({
            nombre: req.body.nombre,
            id_dueno: idUser
        });
        // Guarda el objeto en BD
        let resultado = await nuevoLabel.save();
        // Retorna el objeto guardado al cliente
        res.json(resultado); //tal vez seria mejor poner un aviso de que fue creado correctamente
    } catch (error) {
        // retorna el error al cliente
        res.status(500).send(error);
    }
});

//eliminar un bookmark
router.delete("/labels/:id_usuario/:id_label", async (req, res) => {
    try {
        const idUser = req.params.id_usuario
        const idLabel = req.params.id_label
        // Elimina la tarea por el id
        const resultado = await Label.deleteOne({ id_dueno: idUser, _id: idLabel  })
        // Retorna la respuesta al cliente
        res.json(resultado);
    } catch (error) {
        // retorna el error al cliente
        res.status(500).send(error);
    }
});

//editar un label
router.patch("/labels/:id_usuario/:id_label", async (req, res) => {
    const idUser = req.params.id_usuario
    const idLabel = req.params.id_label
    try {
        
        const resultado = await Label.updateOne(
            { id_dueno: idUser, _id: idLabel },
            {
                $set: req.body,
            }
        ).exec();
        // Retorna la respuesta al cliente
        return res.json(resultado);
    } catch (error) {
        return res.status(500).send(error);
    }
});

module.exports = router;
