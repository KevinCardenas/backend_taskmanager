const express = require("express");
const Usuario = require("../models/usuarios");

// Creación del router
const router = express.Router();

//METODO TEMPORAL PARA OBTENER TODOS LOS USUARIOS
router.get("/users", async (req, res) => {
    try {
        const us = await Usuario.find()

        // Retorna el objeto guardado al cliente
        res.json(us); //tal vez seria mejor poner un aviso de que fue creado correctamente
    } catch (error) {
        // retorna el error al cliente
        res.status(500).send(error);
    }
});

//registro de usuario
router.post("/registro", async (req, res) => {
    try {
        // Crea el objeto
        const nuevoUsuario = new Usuario({
            username: req.body.username,
            password: req.body.password,
            nombre: req.body.nombre,
            email: req.body.email,
            fecha: Date.now()
        });
        // Guarda el objeto en BD
        let resultado = await nuevoUsuario.save();
        // Retorna el objeto guardado al cliente
        res.json(resultado); //tal vez seria mejor poner un aviso de que fue creado correctamente
    } catch (error) {
        // retorna el error al cliente
        res.status(500).send(error);
    }
});

//login de usuario
router.post("/login", async (req, res) => {
    try {
        const usuarios_db = await Usuario.find().exec(); //JSON de usuarios
        const num_users = usuarios_db.length; //cantidad de usuarios

        const us = req.body.username;
        const pw = req.body.password;
        
        var resp = false;
        for (i = 0; i < num_users; i++) {
            if (us === usuarios_db[i].username) {
                console.log(usuarios_db[i]);
                if (pw === usuarios_db[i].password) {
                    resp = usuarios_db[i]
                }
            }
        }
        return res.json(resp)
    } catch (error) {
        // retorna el error al cliente
        res.status(500).send(error);
    }
});

module.exports = router;
