// Importar express
const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");

const usuariosRouter = require('./routes/usuarios')
const bookmarksRouter = require('./routes/bookmarks')
const labelsRouter = require('./routes/labels')

// Creación de la aplicación
const app = express();
var morgan = require('morgan')

// Middlewares
app.use(bodyParser.json());
app.use(morgan('dev'))
app.use(cors())

// Middlewares de rutas
app.use('/', usuariosRouter); //ruta de usuarios
app.use('/', bookmarksRouter); //ruta de bookmarks
app.use('/', labelsRouter); //ruta de labels

// Endpoint inicial
app.get("/", (req, res) => {
    res.send("<h1>PAGINA INICIAL XD</h1>");
});

// Conexión a la base de datos
mongoose.connect("mongodb+srv://admin_tareas:admin_tareas@clustertareas.dhkre.mongodb.net/ProyectoTareas?retryWrites=true&w=majority",
    { useUnifiedTopology: true, useNewUrlParser: true },
    () => {
        console.log("Conectado a la base de datos...");
    }
);

// Inicio del servidor
app.listen(2000);
