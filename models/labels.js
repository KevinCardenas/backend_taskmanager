const mongoose = require('mongoose');

const labelsSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    id_dueno: String
})

module.exports = mongoose.model('labels_db', labelsSchema);