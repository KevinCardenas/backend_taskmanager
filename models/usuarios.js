const mongoose = require("mongoose");

const UsuarioSchema = mongoose.Schema({
  username: {
    type: String,
    required: true,
  },
  password: { //debe quedar encriptado
    type: String,
    required: true,
  },
  nombre: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  fecha: {
    type: Date,
    required: true
  }
});

// Exportar el esquema
module.exports = mongoose.model("Usuarios_DB", UsuarioSchema);
