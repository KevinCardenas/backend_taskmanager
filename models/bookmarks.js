const mongoose = require('mongoose');

// Definición del esquema
const bookmarksSchema = mongoose.Schema({
    nombre: {
        type: String,
        required: true
    },
    url: {
        type: String,
        required: true
    },
    id_label: String,
    comentario: String,
    user_book: String,
    pass_book: String,
    id_dueno: String
})

// Exportar el esquema
module.exports = mongoose.model('bookmarks_db', bookmarksSchema);