const axios = require("axios");

var url_backend = "http://localhost:2000"

// LOGIN
async function iniciarSesion() {
  try {
    var respuesta = await axios.post(url_backend + "/login", {
      username: "sebastiansanabria",
      password: "12"
    });
    console.log(respuesta.data);
  } catch (error) {
    console.log(respuesta);
  }
}

//REGISTRO
async function registrarUsuario(objetoUser) {
  try {
    var respuesta = await axios.post(url_backend + "/registro", {
      "username":"hector julio",
      "password":"israel123",
      "nombre":"irak456",
      "email":"cohthgo@g.com"
    });
    console.log(respuesta.data);
  } catch (error) {
    console.log(respuesta);
  }
}

//VER TODOS LOS USUARIOS
async function obtenerUsuaros() {
  try {
    let respuesta = await axios.get(url_backend + "/users");
    console.log(respuesta.data);
  } catch (error) {
    console.log(error);
  }
}

//CREAR BOOKMARK
async function crearBookmark() {
  try {
    let respuesta = await axios.post(url_backend + "/bookmarks/" + idUser, {
      "nombre": "isabellla",
      "url": "www.facebookñ.com",
      "comentario": "admin123",
      "labels": [{
        "_id": "5fb1545c14e85920e4907af3",
        "nombre": "LABEL DE KEVIN",
        "id_dueno": "5fb15068ab90f40f4ce71d85",
        "__v": 0
      }],
      "user_book": "user",
      "pass_book": "pass"
    });
    console.log(respuesta.data);
  } catch (error) {
    console.log(respuesta);
  }
}

//CREAR LABEL
async function crearLabel() {
  try {
    let respuesta = await axios.post(url_backend + "/labels/" + idUser, {
      "nombre": "hola que hace pa 2"
    });
    console.log(respuesta.data);
  } catch (error) {
    console.log(respuesta);
  }
}

//VER TODOS LOS BOOKMARKS
async function verTodosBookmarks() {
  try {
    let resultado = await axios.get(url_backend + "/bookmarks/"+ idUser);
    console.log(resultado.data);
  } catch (error) {
    console.log(error);
  }
}

//VER TODOS LOS LABELS
async function verTodosLabels() {
  try {
    let resultado = await axios.get(url_backend + "/labels/"+ idUser);
    console.log(resultado.data);
  } catch (error) {
    console.log(error);
  }
}

//VER UN SOLO LABEL
async function verUnLabel() {
  try {
    let resultado = await axios.get(url_backend + "/labels/"+ idUser+"/" + idLabel);
    console.log(resultado.data);
  } catch (error) {
    console.log(error);
  }
}

//VER UN SOLO BOOKMARK
async function verUnBookmark() {
  try {
    let resultado = await axios.get(url_backend + "/bookmarks/"+ idUser+"/" + idBookmark);
    console.log(resultado.data);
  } catch (error) {
    console.log(error);
  }
}

//EDITAR BOOKMARK
async function modificarBookmark() {
  try {
    let respuesta = await axios.patch(`${url_backend}/bookmarks/${idUser}/${idBookmark}`, { "comentario": "EDIT BOOK DESDE AXIOS" });
    console.log(respuesta.data);
  } catch (error) {
    console.log(error);
  }
}

//EDITAR LABEL
async function modificarLabel() {
  try {
    let respuesta = await axios.patch(`${url_backend}/labels/${idUser}/${idLabel}`, { "nombre": "EDIT L DESDE AXIOS" });
    console.log(respuesta.data);
  } catch (error) {
    console.log(error);
  }
}

//ELIMINAR BOOKMARK
async function eliminarBookmark() {
  try {
    let respuesta = await axios.delete(`${url_backend}/bookmarks/${idUser}/${idBookmark}`);
    console.log(respuesta.data);
  } catch (error) {
    console.log(error);
  }
}

//ELIMINAR LABEL
async function eliminarLabel() {
  try {
    let respuesta = await axios.delete(`${url_backend}/labels/${idUser}/${idLabel}`);
    console.log(respuesta.data);
  } catch (error) {
    console.log(error);
  }
}

//VER BOOKMARKS DE UN LABEL
async function verBookmarksDeLabel() {
  try {
    let resultado = await axios.get(url_backend + "/bookmarks/porlabel/"+ idUser+"/"+idLabel);
    console.log(resultado.data);
  } catch (error) {
    console.log(error);
  }
}

//iniciarSesion()
//obtenerUsuaros()
//crearBookmark()
//crearLabel()
//verTodosBookmarks()
//verTodosLabels()
//verUnLaabel()
//verUnBookmark()
//modificarBookmark()
//modificarLabel()
//eliminarBookmark()
//eliminarLabel()
//verBookmarksDeLabel()
//registrarUsuario()