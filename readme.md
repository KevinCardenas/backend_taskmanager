
# Backend del administrador de tareas

Proyecto hecho en:
- Nodejs
- Express
- Mongodb
- Mongoose

## Instalación

Para instalar este proyecto ejecute:

```bash
npm install
```

## Ejecución

Para ejecutar este proyecto ejecute:

```bash
npm start
```